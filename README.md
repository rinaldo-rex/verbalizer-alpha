# Verbalizer
The application is command line only, and runs with python2.7 as of now.

Please note that the requirements for the project are mentioned under requirements.txt (Some of the modules may not be relevant, because not all the intended features have been enabled in the application)

The starting point of the program would be executer.py

The other files will perform corresponding functions such as...

* browser_handler.py - Handles browswer functions
* classifiers*.py - Sentence and class identification (What kind of activity was intended?)
* clock.py - Fuzzy clock for natural language output for time
* dbhandler.py - stores into mongodb for further uses
* executer.py - Start of the application (executes other actions including parsing and classifications of user inputs)
* gmail_accessor.py - Gmail check
* gmail_check.py - Gmail check
* goodreads_books.py - For fetching book review/other book related information from goodreads
* input_analyzer*.py - The module to analyze user inputs
* movie_tv_helper.py - Fetches reviews/movie/tv series information just like goodreads
* notifier.py - The notification modules that pushes info to desktop notification
* quotes.py - Fetches random quotes
* reminder.py - Reminder module to remind user of tasks
* trainer.py - To train the naive bayes classifier to increase the accuracy.
* weather.py - Weather reports.