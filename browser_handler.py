"""Module to help access browser through python selenium api"""

from selenium import webdriver

browser = webdriver.Chrome("/home/rinaldo/verbalizer/chromedriver") #opens chrome

def news():
    """Function to open google news"""
    browser.get("https://news.google.com")

def gmail():
    """Opens gmail inbox"""
    browser.get("https://mail.google.com")

def fb():
    """Opens facebook"""
    browser.get("https://facebook.com")