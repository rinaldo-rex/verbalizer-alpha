#this module is used to train a simple Naive bayes classifier to classify sentences based on some sample sentences

from textblob.classifiers import NaiveBayesClassifier # a naive bayes classifier is a self learning algorithm for identifying and classifying sentence types
import sqlite3 #for storing a history of user input sentences for learning enhancement

#conn = sqlite3.connect('sent_ins.db') #the database
# print "Connection to database successful"

#conn.execute('''CREATE TABLE IF NOT EXISTS sentences
#(sent TEXT, sent_type TEXT);''')


training_sentences = [
    ('What is the time now?', 'time'),
    ('Play the next song, will you?', 'media_song'),
    ("I've got too much to do, damn.", 'work'),
    ('Suggest me a movie buddy', 'media_movie'),
    ("Pfft, I'm bored", 'bored'),
    ('Check the weather for me', 'weather'),
    ("Hey dude, what's a platypus?", 'definition'),
    ("Remind me in a few hours for work", 'reminder'),
    ("Check my mail, bud", 'mail'),
    ("Can you give me the driving directions to ", 'directions_map')
]

classifier = NaiveBayesClassifier(training_sentences)

def type(textin, np):
    """Function that returns the type of sentence/noun_phrase"""
    #conn.execute('''INSERT INTO sentences (sent,sent_type) VALUES (?,?)''',(textin, str(classifier.classify(textin))))
    return classifier.classify(textin)

def close_db():
    """This function enables committing to the database and closing the connection gracefully"""
    #conn.close()
