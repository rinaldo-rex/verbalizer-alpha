"""
This is the initial classification module for basic group identification
"""
from textblob.classifiers import NaiveBayesClassifier #for a Naive classifier
import dbhandler #for handling training data from/to the db
import pprint
# from nltk.corpus import stopwords #for filtering stopwords
# from spacy.en import English
# parser = English(tagger=False, entity=False)
dbhandle = dbhandler.Handler()
# stopw = list(stopwords.words('english'))
# cl = NaiveBayesClassifier()

class Classifier:
    """Classifier module"""

    def __init__(self):
        """Initialize the classifier with the training data"""

        self.train() #to train the classifer

    def train(self):
        """Method to train the classifier"""
        # dbhandle.train_update("Recommend me a movie", "media_movie")
        training_data = dbhandle.train()
        tdata = [
 ('Can fetch reviews movie The Revenant?', 'media_movie'),
 ("I'm bored, play good flick!", 'media_movie'),
 ('Suggest good thriller', 'media_movie'),
 ("What's time now?", 'datetime_time'),
 ('Time pls, bud?', 'datetime_time'),
 ('Tell time dude', 'datetime_time'),
 ("What's platypus?", 'definition_word'),
 ('Can help define asteroid?', 'definition_word'),
 ("What's mean 'call off'?", 'definition_word'),
 ("Hey bud, where's Arizona?", 'map_location'),
 ('I need map America', 'map_location'),
 ('Can pinpoint Yugoslavia me?', 'map_location'),
 ('I need go Chennai now...', 'map_directions'),
 ('How I get Mumbai here?', 'map_directions'),
 ('Help navigate Coimbatore...', 'map_directions'),
 ('I need sort schedule', 'work'),
 ("God, I've got much do!", 'work'),
 ('Hey, help organize stuff...', 'work'),
 ('Have I got mails bud?', 'mail'),
 ('I need send mail abdulqaathir@gmail.com', 'mail'),
 ('Open gmail...', 'mail'),
 ('Got notifications fb?', 'social_network'),
 ('Update status facebook, you?', 'social_network'),
 ("Damn, I can't get quote head!", 'social_network'),
 ("What's weather like?", 'weather'),
 ('Will rain tomo?', 'weather'),
 ("Shit, it's hot today...", 'weather'),
 ('Tell good quote?', 'quotes'),
 ('Motivate me!', 'quotes'),
 ('Can fetch good quotation?', 'quotes')
]
        # pprint.pprint(training_data)
        # for tup in training_data:
        #     for token in parser(tup[0]):
        #         if token.orth_ in stopw:continue
        #         print token,
        #     print
        self.cl = NaiveBayesClassifier(tdata)

        while True:
            text = raw_input(">")
            if text == "Exit": break
            print self.cl.classify(text)
            print self.cl.show_informative_features()

    def type(self, textin, np):
        """Method that classifies the input text(textin) and the noun phrases(np)"""
        return (textin, self.cl.classify(textin), self.cl.classify(np))


if __name__ == '__main__':
    C = Classifier()
    C.train()
