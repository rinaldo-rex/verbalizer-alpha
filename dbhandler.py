from pymongo import MongoClient
from datetime import datetime #for date and time
from nltk.corpus import stopwords #for filtering stop words in training data
import pprint

client = MongoClient("mongodb://127.0.0.1:21212") #this is the port used to start mongod
db = client['verb_try']

class Handler:
    """This class is the handler for all db operations throughout Verbalizer's operations"""

    def __init__(self):
        """Initializes the database"""
        #use sudo mongod --port 21212 --dbpath /home/rinaldo/verbalizer/MongoDb\ try to start the mongo instance

    def sent_add(self, sentence):
        """Function to add the input sentence into the input_log"""
        result = db.input_log.insert_one(
            {
                "raw_string": sentence,
                "datetime": datetime.now()
            }
        )

    def parsed_add(self, sent, nps, type, sent_class):
        """Function to add the parsed sentence and it's features to the processed_log"""
        result = db.processed_log.insert_one(
            {
                "raw_string":sent,
                "features":{
                    "nps": nps,
                    "type":type,
                    "sent_class":sent_class
                }
            }
        )

    def train_update(self, sent, sent_class):
        """adds training data to the classifier"""
        print type(sent)
        result = db.train_data.update_many(
            {"class":sent_class},
            {"$push":{"data":sent}
            },
            upsert=True
        )


    def train(self):
        """Method to fetch training data"""
        sw = stopwords.words('english')
        training_data = []
        cursor = db.train_data.find({}, {"_id":0, "class":1, "data":1})
        for doc in cursor:
            for d in doc['data']:
                words = [i for i in str(d).split() if i not in sw]
                sentence = ' '.join(word for word in words).strip()
                training_data.append((sentence,doc['class'].encode('ascii')))
        pprint.pprint(training_data)
        print "Training the cl: ", len(training_data)
        for tup in training_data:
            print type(tup[0]), type(tup[1])
        return training_data
