"""Module to take care of executing the operations/actions"""
import vlc # for handling media_movie, media_audio
import weather
import notifier
import time
import clock # for fuzzy time

print "LOADING. Please wait..."
from spacy.en import English # for exceptional processing
Parser = English()
N = notifier.Notifier()
vlc_instance = vlc.Instance('--fullscreen', '--interact', '--play-and-exit')
player = vlc_instance.media_player_new()
media = vlc_instance.media_new('file:///home//rinaldo//ruby.mkv')
player.set_media(media)
class Executer:
    """Class with methods pertaining to actions"""

    def __init__(self):
        """Initialization of the class"""

    def caller(self, raw_string, class_sent, class_np):
        """Method to compartmentalize the actions based on the class"""
        action_class = class_sent
        if action_class == "media_movie":
            self.action_movie(raw_string)
        elif action_class == "media_audio":
            self.action_audio(raw_string)
        elif action_class == "datetime_time": self.action_time(raw_string)
        elif action_class == "datetime_date": self.action_date(raw_string)

    def action_movie(self, sent_in):
        """Method to handle media_movie class requests"""
        positional_adj = ['next', 'previous', 'prev']
        genre_adj = ['romance', 'thriller', 'action', 'funny']
        generic_adj = ['good', 'bad']
        sentence = unicode(sent_in, 'utf-8')
        doc = Parser(sentence)
        for np in doc.noun_chunks:
            print("NP: ", np.text)
            for tok in np:
                print tok
                if tok.orth_ in positional_adj: print "Playing: ", tok.orth_
                if tok.orth_ in genre_adj: print "In Genre: ", tok.orth_
                if tok.orth_ in generic_adj: print "Some ", tok.orth_, " movie!"
        for token in doc:
            print token.orth_, token.pos_



        if "play" in sent_in:
            if (not player.will_play()) and (not player.can_pause()):
                player.play()
                N.notify("Verbalizer - Media Player", str(media.get_mrl()), "media-playback-start")
        if "pause" in sent_in:
            if player.can_pause():
                player.pause()
                N.notify("Verbalizer - Media Player","Media paused", "media-playback-pause")
        if "stop" in sent_in:
            if player.can_pause():
                player.stop()
                N.notify("Verbalizer - Media Player","Media stopped!", "media-playback-stop")


    def action_audio(self):
        """Method to handle media_audio class"""

    def action_time(self):
        """Method to handle datetime_time class"""

    def action_date(self):
        """Method to handle datetime_date class"""

    def action_weather(self):
        """for weather forecast"""
        rep = weather.report()
        N.notify("Verbalizer - Weather", rep, "weather-showers")

    def action_time(self):
        """For time update"""
        stm = time.localtime()
        if (stm.tm_hour == 0) and (stm.tm_min > 30): hour = 0
        else: hour = stm.tm_hour

        if (stm.tm_hour == 24) and (stm.tm_min<=30): hour = 24
        else: hour = stm.tm_hour
        fuzzy_time = clock.fuzzy(hour, stm.tm_min)
        N.notify("Verbalizer - Clock", fuzzy_time, "appointment-new")

if __name__ == '__main__':
    E = Executer()
    while True:
        # weather.report()
        t = raw_input("> ")
        if t == 'Exit': break
        # weather.report()
        if "movie" in t:
            print "-------MOVIE CLASS-----"
            E.action_movie(t)
        elif "weather" in t:
            print "-------WEATHER CLASS-----"
            E.action_weather()
        elif "time" in t:
            print "-------TIME CLASS-----"
            E.action_time()