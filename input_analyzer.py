print "Please wait --- Loading libraries"

from spacy.en import English #for high-speed tokenizing and tagging
parser = English() #actual parser at work. Takes about 20 seconds to load fully.
import classifiers #for classifying the parsed sentence through a naive bayes classifier
import weather #for weather forecast
import datetime
print "LOAD COMPLETE"

class Parse_input:
    """Parse_input parses the given input for tpkens and identifying noun_phrases"""
    input_sentence = ""

    def __init__(self):
        """Initialize the object by loading a sample data"""
        # input_sentence = raw_input(">") #user input via keyboard as of now

    def find_nps(self, sentence=""):
        """Fn to find Nound phrases"""
        if sentence == "":
            sentence = raw_input(">")

        sentence = unicode(sentence, 'utf-8')
        doc = parser(sentence)
        for np in doc.noun_chunks:
            print("NP: ", np.text)
            print "\n"
            sent_type = str(classifiers.type(str(sentence),str(np)))
            print sent_type
            if 'weather' in sent_type: #for getting weather updates
                weather.report()
            if 'time' in sent_type:
                print datetime.datetime.now()


        self.find_tags(sentence)

    def find_tags(self, sentence):
        """Fn to find the POS tags for the given sentence"""

        for token in parser(sentence):
            print("POS_TAG: ", token.orth_, token.pos_)
            print "\n"

if __name__ == '__main__':
    P = Parse_input()
    P.find_nps(P.input_sentence)
    while (raw_input(">")!="exit"):
        P.find_nps()
    #classifiers.close_db()


