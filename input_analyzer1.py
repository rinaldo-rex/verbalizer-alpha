"""
Input analyzer is the first to execute. Get's the input sentences, logs it into the input_log NoSql collection.
Also this is where the the input is parsed and classified for further processing
"""
from spacy.en import English
import dbhandler #for handling db connection and operations
# import responder #module to handle responses for special cases TODO

parser = English() #takes about twenty seconds to load.
global raw_string

class Initializer:
    """Class to initialize stuff, TODO"""

class Analyzer:
    """Class for analyzing the user input via text"""
    def __init__(self):
        """Dummy initialization"""

    def parse_add(self, sentence):
        """adds content of the input sentence to the test collection"""


    def getInput(self):
        """Method to get input sentences from the user"""
        while True:
            raw_string = raw_input(">")
            if raw_string == "Exit":
                print "Thank you. Bye!"
                break
            else:
                dbhandle = dbhandler.Handler()
                dbhandle.sent_add(raw_string)
                self.parse_add(raw_string)

if __name__ == '__main__':

    A = Analyzer()
    A.getInput()