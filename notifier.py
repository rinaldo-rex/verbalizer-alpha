"""Module to notify the user via notificaiton"""
import pgi
pgi.require_version('Notify', '0.7')
from pgi.repository import Notify
# global N = None
class Notifier:
    def __init__(self):
        Notify.init("Verbalizer")

    def notify(self, title, summary, icon="dialog-information", timeout=5000):
        """Method to notify the OS"""

        Ni = Notify.Notification.new(title, summary, icon)
        if Notify.is_initted():
            Ni.update(title, summary, icon)
            Ni.show()
        else:
            Ni.show()
