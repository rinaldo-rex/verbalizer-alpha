"""Module to fetch random quotes with author"""
from forismatic import Forismatic
import notifier

N = notifier.Notifier()
class Quoter:

    def __init__(self):
        self.f = Forismatic()

    def fetch_quote(self):
        """Fetches random quote and displays it in the notification"""
        quote = self.f.get_quote()
        q = "" + quote.quote + "\n" + " --" + quote.author
        N.notify("Food for thought!",q, "food-cake", timeout=1)

if __name__ == '__main__':
    Q = Quoter()
    Q.fetch_quote()