"""Module that schedules a one time task such as the day's reminders"""
import subprocess
from spacy.en import English
from nltk.corpus import stopwords

parser = English()
sw = stopwords.words('english')
def add_reminder(text):
    """Fn. that adds the reminder using at"""
    reminder = filter_task(text) #task
    time = filter_time(text) #time
    if 'at' in text: job = ['at', time]
    else: job = ['at', 'now + ' + time]
    p = subprocess.Popen(job, stdin=subprocess.PIPE)
    p.communicate(reminder)

def filter_task(text):
    """Filters the task properly"""
    text = unicode(text)
    task = ""
    words = text.split()
    kw = ['to','for'] #to help identify task
    kw2 = ['in', 'after', 'about'] #to filter time info
    for k in kw:
        if k in words:
            task = " ".join(words[words.index(k) + 1:])
            break
    words = task.split()
    for k in kw2:
        if k in words:
            task = " ".join(words[:words.index(k)])
    print task.capitalize()
    return task.capitalize()


def filter_time(text):
    """Filters the time properly"""
    text = unicode(text, 'utf-8')

    date = ""
    time = ""
    for token in parser(text):
        if token.ent_type_ == 'DATE':
            # print "Found date. Add to .reminders", token.orth_
            date += token.orth_ + " "
        elif token.ent_type_ == 'TIME':
            # print "Found time. Execute 'at'", token.orth_
            time += token.orth_ + " "
        # elif token.ent_type_ == 'PERSON':
        #     print "Found name", token.orth_
        else: pass

    # sw.extend(['in', 'and', 'at'])
    # for w in sw:
    #     time = time.replace(w,'')
    flag = True
    if 'an' in time:
        time = time.replace('an','1')
        flag = False
    elif 'a' in time:
        time = time.replace('a', '1')
        flag = False
    if 'few' in time:
        if flag:
            time = time.replace('few', '2')
        else: time = time.replace('few','')

    date.strip()
    time.strip()
    # print date
    # print time

    return time
if __name__ == '__main__':
    add_reminder(raw_input('>'))