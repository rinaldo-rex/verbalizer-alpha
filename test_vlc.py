import vlc
from vlc import VLCException

i = vlc.Instance('--no-fullscreen', '--interact', '--play-and-exit')
e = VLCException()
p = i.media_player_new()
m = i.media_new('file:///home//rinaldo//ruby.mkv')
p.set_media(m)
while True:
    t = raw_input("> ")
    if t == "play": p.play()
    elif t == "pause": p.pause()
    elif t == "stop": p.stop()
    else: break