"""
Module to help add training sentences to the db
"""

import dbhandler #for update function

def add(sent, cl):
    print "Sure? ", (sent, cl)
    if raw_input(">") == 'y':
        dbhandle = dbhandler.Handler()
        dbhandle.train_update(sent, cl)
    else: pass


while True:
    t = raw_input("Sentence: ")
    t1 = "unknown"
    if t == "Exit":
        break
    else:
        t1 = raw_input("Class: ")
        add(t, t1)
