import forecastio #the forecastio wrapper for the API

api_key = "7b030385ddf0266647285c01fb4d9fa8"#API key for forecastio
lat = 10.9404984
long = 76.7423094 #lat, long provided by default for Karunya Univ

forecast = forecastio.load_forecast(api_key, lat, long) #loads current forecast

def report():
    """This method returns the summary for the weather forecast"""
    daily = forecast.daily()
    return daily.summary

def hourly():
    """Provides hourly updates"""
    byHour = forecast.hourly()
    print byHour.summary
    for hourlyData in byHour.data:
        print hourlyData.temperature